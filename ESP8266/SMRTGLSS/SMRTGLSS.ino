/****************** Includes ************************/
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "index_html.h"

/****************** Const & Defines *****************/
#define USER_LED  (5)              /*!< Blue led    */
#define USER_BAUD (115200)         /*!< UART 115200 */
#define USER_SSID ("IZZI-2742")    /*!< SSID        */
#define USER_PASS ("509551A42742") /*!< Password    */
#define USER_PORT (80) /*!< Web server in Port #80  */
#define USER_TIME (500)/*!< Connection Delay (ms)   */
const char* PARAM_INPUT = "value";/*!< Slider Param */

/************ Global Variables  *********************/
AsyncWebServer server(USER_PORT); /*!< Web server   */
extern const char index_html[];   /*!< HTML Page    */
int Transparency = 0; /*!< Transparency (Manual)    */
int OperationMode =0; /*!< Operation Control Mode   */

/************ Public Prototypes *********************/
String processor(const String& var) { return String(); }

/************ Initialization ************************/
void setup ()
{
  /* Set-up Serial communication */
  Serial.begin(USER_BAUD);
  Serial1.begin(9600);
  pinMode(USER_LED, OUTPUT);
  digitalWrite(USER_LED, 0);

  /* Set-up the WiFi connection */
  Serial.print("Connecting to ");
  Serial.println((const char *)USER_SSID);
  WiFi.begin((const char *)USER_SSID, (const char *)USER_PASS);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(USER_TIME);
    Serial.print(".");
  }

  /* Print local IP address and start web server */
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  /* Route for root / web page */
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
  {
    request->send_P(200, "text/html", index_html, processor);
  });

  /* Send a GET request to <ESP_IP>/slider1?value=<inputMessage> */
  server.on("/slider1", HTTP_GET, [] (AsyncWebServerRequest *request)
  {
    String inputMessage;
    
    /* GET input1 value on <ESP_IP>/slider1?value=<inputMessage> */
    if (request->hasParam(PARAM_INPUT))
    {
      inputMessage = request->getParam(PARAM_INPUT)->value();
      Transparency = inputMessage.toInt();
    }
    else
    {
      inputMessage = "No message sent";
    }
    /* Serial.println(inputMessage); */
    request->send(200, "text/plain", "OK");
  });

  /* Start server */
  server.begin();
}

/************ Endless loop **************************/
void loop()
{
  if(Transparency == 0) {
    digitalWrite(USER_LED, 0);
  }
  else {
    digitalWrite(USER_LED, 1);
    Serial1.write(Transparency);
  }
  
  delay(600);
}
