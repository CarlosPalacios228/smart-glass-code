/****************** Includes ************************/
#include "index_html.h"

/************ HTML Style ****************************/
const char index_html[] = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <title>SMART GLASS</title>
  <style>
    html {
      font-family: Arial;
      display: inline-block;
      text-align: center;
      }
    h1 {font-size: 50px;}
    h2 {font-size: 25px;}
    p  {font-size: 20px;}
    body {
      max-width: 400px; 
      margin:0px auto; 
      padding-bottom: 25px;
      }
    .slider {
      -webkit-appearance: none; border-radius: 10px;
      margin: 14px; width: 360px; height: 25px; background: #d3d3d3;
      outline: none; -webkit-transition: .2s; transition: opacity .2s;
      }
    .slider::-webkit-slider-thumb {
      -webkit-appearance: none; appearance: none; 
      width: 35px; height: 35px; background: #bf800a; border-radius: 50%;
      cursor: pointer;
      }
    .slider::-moz-range-thumb { 
      width: 35px; height: 35px; background: #4caf50; border-radius: 50%;
      cursor: pointer; 
      }
    .smg-headline{
    padding-top: 50px;
    padding-bottom: auto;
    text-align: center;
    }
    .smg-headline > h1 {
    color: indigo;
    }
    .smg-headline > img {
    width: 250px;
    height: auto;
    margin-top: 20px;
    margin-bottom: 15px;
    }
    .smg-btns{
    text-align: center;
    margin-bottom: 25px;
    }
    .smg-slider > p{
    color: indigo;
    }
   
  /*the container must be positioned relative:*/
  .custom-select {
  position: relative;
  font-family: Arial;
  }

  .custom-select select {
  display: none; /*hide original SELECT element:*/
  }

  .select-selected {
  //background-color: indigo;
  }

  /*style the arrow inside the select element:*/
  .select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
  }

  /*point the arrow upwards when the select box is open (active):*/
  .select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
  }

  /*style the items (options), including the selected item:*/
  .select-items div,.select-selected {
  color: #000;
  //padding: 8px 16px;
  border: 1px solid transparent;
  border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
  cursor: pointer;
  user-select: none;
  }

  /*style items (options):*/
  .select-items {
  position: absolute;
  //background-color: DodgerBlue;
  top: 100%;
  left: 0;
  right: 0;
  z-index: 99;
  }

  /*hide the items when the select box is closed:*/
  .select-hide {
  display: none;
  }

  .select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
  }
  .smg-btn-window{
  text-align:center;
  margin-bottom: 30px;
  }
  </style>
</head>
<body>
<section id="smart_glass" class="container-fluid">

  <div class="row">
    <div class="col-12 smg-headline">
      <h1>SMART GLASS</h1>
      <h2>The future of lighting</h2>
      <img src="https://www.flaticon.com/svg/static/icons/svg/55/55283.svg" class="img-fluid" alt="Responsive image">
    </div>
  </div>
  
  <div class="smg-slider">
    <h2>Desired transparency Level</h2>
    <p>Scroll down to zero to activate the automatic mode.</p>
    <h2>Transparency:   <span id="textSliderValue1">%SLIDERVALUE1%</span></h2>
    <p><input type="range" onchange="updateSliderPWM1(this)" id="pwmSlider1" min="0" max="100" value="%SLIDERVALUE1%" step="5" class="slider"></p>
   </div>
   
   <div class="smg-btn-window">
  <h2>Window transparency Level</h2>
<div class="custom-select" style="width:200px;">
  <select>
    <option value="0">Select Window:</option>
    <option value="1">Front Right</option>
    <option value="2">Front Left</option>
    <option value="3">Windshield</option>
    <option value="4">Rear Right</option>
    <option value="5">Rear Left</option>
  <option value="6">Rear</option>
  </select>
</div>
   </div>
  </section>
  
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script>
function updateSliderPWM1(element) {
  var sliderValue1 = document.getElementById("pwmSlider1").value;
  document.getElementById("textSliderValue1").innerHTML = sliderValue1;
  console.log(sliderValue1);
  var xhr1 = new XMLHttpRequest();
  xhr1.open("GET", "/slider1?value="+sliderValue1, true);
  xhr1.send();
}
</script>
<script>
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>
</body>
</html>
)rawliteral";
