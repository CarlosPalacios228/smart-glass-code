################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Adc.c \
../src/Clocking.c \
../src/PID.c \
../src/Pins.c \
../src/Timer.c \
../src/Uart.c \
../src/main.c 

OBJS += \
./src/Adc.o \
./src/Clocking.o \
./src/PID.o \
./src/Pins.o \
./src/Timer.o \
./src/Uart.o \
./src/main.o 

C_DEPS += \
./src/Adc.d \
./src/Clocking.d \
./src/PID.d \
./src/Pins.d \
./src/Timer.d \
./src/Uart.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@src/Adc.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


