/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Adc.h"

/**********************************************************
 * Configure ADC (Potentiometer)
 *********************************************************/
void ADC_Setup(void)
{
  PCC->PCCn[PCC_ADC0_INDEX] &=~ PCC_PCCn_CGC_MASK;  /* Disable clock to change PCS */
  PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_PCS(1);     /* PCS=1: Select SOSCDIV2      */
  PCC->PCCn[PCC_ADC0_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable bus clock in ADC     */

  /* 12-bits, No interrupts */
  ADC0->SC1[0] |= ADC_SC1_ADCH_MASK;
  ADC0->CFG1 |= ADC_CFG1_ADIV_MASK | ADC_CFG1_MODE(1);

  /* Sample in 13 clock cycles, No calibration needed. */
  ADC0->CFG2 = ADC_CFG2_SMPLTS(12);
  ADC0->SC2 = 0x00000000;
  ADC0->SC3 = 0x00000000;
}

/**********************************************************
 * Start ADC Conversion
 *********************************************************/
void ADC_Convert(char channel)
{
  ADC0->SC1[0]&=~ADC_SC1_ADCH_MASK;     /* Clear prior ADCH bits */
  ADC0->SC1[0] = ADC_SC1_ADCH(channel); /* Initiate Conversion   */
}

/**********************************************************
 * Is ADC Conversion Complete ?
 *********************************************************/
char ADC_Complete(void)
{
/* Return COCO */
return ((ADC0->SC1[0] & ADC_SC1_COCO_MASK) >> ADC_SC1_COCO_SHIFT);
}

/**********************************************************
 * Read ADC Result & Compute corresponding Voltage
 *********************************************************/
int  ADC_Read(void)
{
  /* For SW trigger mode, R[0] is used 	*/
  uint16_t adc_result = ADC0->R[0];
return  (uint32_t) ((5000 * adc_result) / 0xFFF);
}

