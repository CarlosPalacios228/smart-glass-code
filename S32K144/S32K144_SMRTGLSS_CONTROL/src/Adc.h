#ifndef ADC_H_
#define ADC_H_
/**********************************************************
 * Public Prototypes
 *********************************************************/
void ADC_Setup(void);
void ADC_Convert(char channel);
char ADC_Complete(void);
int  ADC_Read(void);

#endif /* ADC_H_ */
