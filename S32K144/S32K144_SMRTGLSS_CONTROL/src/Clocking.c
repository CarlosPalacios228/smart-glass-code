/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Clocking.h"

/**********************************************************
 * Option 2. Normal RUN at 80MHz from PLL.
 *********************************************************/
void NormalRunMode_CoreAt80MHz (void)
{
  /* Disable Watch-Dog */
  WDOG->CNT=0xD928C520;
  WDOG->TOVAL=0x0000FFFF;
  WDOG->CS = 0x00002100;

  /* Enable SIRC dividers at 1MHz */
  SCG->SIRCDIV = SCG_SIRCDIV_SIRCDIV1(4) | SCG_SIRCDIV_SIRCDIV2(4);
  SCG->FIRCDIV = SCG_FIRCDIV_FIRCDIV1(1) | SCG_FIRCDIV_FIRCDIV2(1);

  /* Set-up SOSC from the external 8MHz XTAL */
  SCG->SOSCDIV = SCG_SOSCDIV_SOSCDIV1(1) | SCG_SOSCDIV_SOSCDIV2(1);
  SCG->SOSCCFG = SCG_SOSCCFG_RANGE(2) | SCG_SOSCCFG_EREFS_MASK;
  while(SCG->SOSCCSR & SCG_SOSCCSR_LK_MASK);  /* Ensure SOSCCSR unlocked */
  SCG->SOSCCSR = SCG_SOSCCSR_SOSCEN_MASK;  /* Enable oscillator */
  while(!(SCG->SOSCCSR & SCG_SOSCCSR_SOSCVLD_MASK));

  /* Configure the PLL at 160MHz */
  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK);
  SCG->SPLLCSR &= ~SCG_SPLLCSR_SPLLEN_MASK;
                  /* SPLLDIV1 = 80MHz */    /* SPLLDIV2 = 40MHz */
  SCG->SPLLDIV |= SCG_SPLLDIV_SPLLDIV1(3) | SCG_SPLLDIV_SPLLDIV2(3);
  SCG->SPLLCFG = SCG_SPLLCFG_MULT(24);
  while(SCG->SPLLCSR & SCG_SPLLCSR_LK_MASK);  /* Ensure SPLLCSR unlocked */
  SCG->SPLLCSR |= SCG_SPLLCSR_SPLLEN_MASK;  /* Enable SPLL */
  while(!(SCG->SPLLCSR & SCG_SPLLCSR_SPLLVLD_MASK));

  /* Switch to Normal 80MHz Mode */
  SCG->RCCR=SCG_RCCR_SCS(6)  /* Select PLL as clock source */
    |SCG_RCCR_DIVCORE(0b01)  /* Core clock = 160/2 MHz = 80 MHz */
    |SCG_RCCR_DIVBUS(0b01)   /* Bus clock = 40 MHz */
    |SCG_RCCR_DIVSLOW(0b10); /* Flash clock = 26.66 MHz */
  while (((SCG->CSR & SCG_CSR_SCS_MASK) >> SCG_CSR_SCS_SHIFT ) != 6) {}
}
