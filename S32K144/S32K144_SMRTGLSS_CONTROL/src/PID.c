/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "PID.h"

/**********************************************************
 * Initialize PID gains
 *********************************************************/
void PID_Init(PIDC *PID, int32_t Kp, int32_t Ki, int32_t Kd, int32_t T)
{
  /* Copy PID gains and Time */
  PID->Kp = Kp;
  PID->Ki = Ki;
  PID->Kd = Kd;
  PID->T  = T;

  /* Duty Cycle must be between 5-21% */
  PID->LMin = 5;
  PID->LMax = 21;

  /* Clear error and output arrays */
  PID->e[0] = 0.0;
  PID->e[1] = 0.0;
  PID->e[2] = 0.0;
  PID->u[0] = 0.0;
  PID->u[1] = 0.0;

  /* Compute coefficients of the Diff. equation */
  PID->K1 = Kp + Ki*T + Kd/T;
  PID->K2 = (-2 * Kd/T) - Kp;
  PID->K3 = Kd/T;
}

/**********************************************************
 * Compute PID loop
 *********************************************************/
int32_t PID_Controller(PIDC *PID, int32_t SetPoint, int32_t Vin)
{
  /* Current error calculation */
  PID->e[0] = SetPoint - Vin;

  /* Compute PID for current Output */
  PID->u[0] = PID->K1 * PID->e[0] +
              PID->K2 * PID->e[1] +
              PID->K3 * PID->e[2] + PID->u[1];

  /* Output Adjust */
  if(PID->u[0] > PID->LMax)  PID->u[0] = PID->LMax;
  if(PID->u[0] < PID->LMin)  PID->u[0] = PID->LMin;

  /* Shift error and output thought their corresponding arrays */
  PID->e[2] = PID->e[1];
  PID->e[1] = PID->e[0];
  PID->u[1] = PID->u[0];

  return (int32_t)PID->u[0];
}
