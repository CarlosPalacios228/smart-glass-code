#ifndef PID_H_
#define PID_H_

/**********************************************************
 * Data Types
 *********************************************************/
typedef struct
{
  int32_t Kp, Ki, Kd; /*!< PID gains     */
  int32_t LMin, LMax; /*!< Output limits */
  int32_t T;          /*!< Time in s     */

  int32_t e[3]; /*!< Current, past and ancestor errors */
  int32_t u[2]; /*!< Current and past Outputs          */

  /*!< Control variables for future iterations         */
  int32_t K1, K2, K3;

} PIDC;

/**********************************************************
 * Public Prototypes
 *********************************************************/
void PID_Init(PIDC *PID, int32_t Kp, int32_t Ki, int32_t Kd, int32_t T);
int32_t PID_Controller(PIDC *PID, int32_t SetPoint, int32_t Vin);

#endif /* PID_H_ */
