/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Pins.h"
#include "Timer.h"

/**********************************************************
 * AC Control
 *********************************************************/
void EnableTRIAC(void)   { PTD->PSOR |= (1 << 7); }
void DisableTRIAC(void)  { PTD->PCOR |= (1 << 7); }
char OperationMODE(void) { return (PTE->PDIR & (1 << 12)) >> 12; }

/**********************************************************
 * Configure required pins.
 *********************************************************/
void PORT_Setup (void)
{
  /* Enable All PORTs */
  PCC -> PCCn[PCC_PORTA_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable clock for PORT A */
  PCC -> PCCn[PCC_PORTB_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable clock for PORT B */
  PCC -> PCCn[PCC_PORTC_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable clock for PORT C */
  PCC -> PCCn[PCC_PORTD_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable clock for PORT D */
  PCC -> PCCn[PCC_PORTE_INDEX] |= PCC_PCCn_CGC_MASK;   /* Enable clock for PORT E */

  /* RGB LED *************************************************/
  PORTD->PCR[0]  = PORT_PCR_MUX(1);  /* Port D0: MUX = GPIO  */
  PORTD->PCR[15] = PORT_PCR_MUX(1);  /* Port D15: MUX = GPIO */
  PORTD->PCR[16] = PORT_PCR_MUX(1);  /* Port D16: MUX = GPIO */
  PTD->PDDR |= 1<<0   /* Port D0:  Data Direction = output */
              |1<<15  /* Port D15: Data Direction = output */
              |1<<16; /* Port D16: Data Direction = output */
  PTD->PSOR |= 1<<0 | 1<<15 | 1<<16; /* Turn off all LEDs  */

  /* AC Control **********************************************/
  /* PTE1: GPIO Input for Zero Crossing Detection */
  PTE -> PDDR &= ~(1 << 1);
  PORTE->PCR[1] = PORT_PCR_MUX(1)
                | PORT_PCR_PFE_MASK
                | PORT_PCR_IRQC(0xA);

  /* PTD7: GPIO Output for TRIAC control */
  PORTD->PCR[7] = PORT_PCR_MUX(1);
  PTD->PDDR |= (1 << 7);
  PTD->PCOR |= (1 << 7);

  /* UART Pins ***********************************************/
  PORTA->PCR[8] |= PORT_PCR_MUX(2); /* Port A8: MUX = ALT2, UART2 RX */
  PORTA->PCR[9] |= PORT_PCR_MUX(2); /* Port A9: MUX = ALT2, UART2 TX */

  /* Operation MODE */
  /* Port E12: MUX = GPIO, input filter enabled */
  PTE->PDDR &= ~(1 << 12);
  PORTE->PCR[12] = PORT_PCR_MUX(1) | PORT_PCR_PFE_MASK;

  /* Enable PTE Interrupt */
  S32_NVIC->ISER[(uint32_t)(PORTE_IRQn) >> 5U] = (uint32_t)(1U << ((uint32_t)(PORTE_IRQn) & (uint32_t)0x1FU));
  S32_NVIC->ICPR[(uint32_t)(PORTE_IRQn) >> 5U] = (uint32_t)(1U << ((uint32_t)(PORTE_IRQn) & (uint32_t)0x1FU));
}

/**********************************************************
 * PTE1 Handler: Zero Crossing Detection
 *********************************************************/
void PORTE_IRQHandler (void)
{
  if(PORTE->PCR[1] & PORT_PCR_ISF_MASK)
  {
    PORTE->PCR[1] |= PORT_PCR_ISF_MASK;
    TimerStart();
  }
}
