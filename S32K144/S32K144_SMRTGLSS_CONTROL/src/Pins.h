#ifndef PINS_H_
#define PINS_H_
/**********************************************************
 * Public Prototypes and Defines
 *********************************************************/
void PORT_Setup (void);
void EnableTRIAC(void);
void DisableTRIAC(void);

#define SMG_AUTO    0
#define SMG_MANUAL  1
char OperationMODE(void);

#endif /* PINS_H_ */
