/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Timer.h"
#include "Pins.h"

/* Max. Period is 8,333us (120Hz from AC) */
#define PIT_MAX_CLK    (8000)//8300 /* Leave 300us for protection */
volatile unsigned int  new_pit_value;
volatile unsigned char update_request;

/**********************************************************
 * Configure Delay for the Optical TRIAC.
 *********************************************************/
void TimerSetup(void)
{
  /* Clock Source = 2 (SIRCDIV2_CLK = 125KHz) */
  PCC -> PCCn[PCC_LPIT_INDEX] = PCC_PCCn_PCS(2);
  PCC -> PCCn[PCC_LPIT_INDEX] |= PCC_PCCn_CGC_MASK;

  /* Channel 0 Timeout period 50% */
  LPIT0 -> MCR |= LPIT_MCR_M_CEN_MASK;
  LPIT0 -> TMR[0].TVAL = PIT_MAX_CLK / 2; // 7200
  update_request = 0;

  /* TIE0 = 1: Timer Interrupt Enabled for Channel 0 */
  LPIT0 -> MIER |= LPIT_MIER_TIE0_MASK;

  /* Enable PIT Interrupt */
  S32_NVIC->ISER[(uint32_t)(LPIT0_Ch0_IRQn) >> 5U] = (uint32_t)(1U << ((uint32_t)(LPIT0_Ch0_IRQn) & (uint32_t)0x1FU));
  S32_NVIC->ICPR[(uint32_t)(LPIT0_Ch0_IRQn) >> 5U] = (uint32_t)(1U << ((uint32_t)(LPIT0_Ch0_IRQn) & (uint32_t)0x1FU));
}

/**********************************************************
 * Timing functions
 *********************************************************/
void TimerUpdate(int duty)
{
  /* Set update flag */
  update_request = 1;

  /* Duty Cycle must be between 5-21% */
  if(duty < 5 ) duty = 5;
  if(duty > 21) duty = 21;

  new_pit_value = (PIT_MAX_CLK * (100-duty)) / 100;
}

void TimerStart(void)
{
  /* Enable module and channel */
  LPIT0 -> MCR |= LPIT_MCR_M_CEN_MASK;
  LPIT0 -> TMR[0].TCTRL |= LPIT_TMR_TCTRL_T_EN_MASK;
}

void TimerStop(void)
{
  /* Disable channel and module */
  LPIT0 -> TMR[0].TCTRL &= ~LPIT_TMR_TCTRL_T_EN_MASK;
  LPIT0 -> MCR &= ~LPIT_MCR_M_CEN_MASK;
}

/**********************************************************
 * PIT Handler
 *********************************************************/
void delay (uint32_t cycles) { while(cycles--)  __asm("NOP"); }
void LPIT0_Ch0_IRQHandler (void)
{
  /* Check the Timeout Interrupt Flag for Channel 0 */
  if ((LPIT0 -> MSR & LPIT_MSR_TIF0_MASK) == LPIT_MSR_TIF0_MASK)
  {
    /* Clear Flag */
    LPIT0 -> MSR |= LPIT_MSR_TIF0_MASK;

    /* Enable the TRIAC during a small portion of the sinusoidal wave */
    EnableTRIAC();

    if(update_request)
    {
      update_request = 0;
      LPIT0 -> TMR[0].TVAL = new_pit_value;
    }
    TimerStop();

    /* Disable TRIAC again... */
    delay(64); /* Do 32 NOP's, ~10us. */
    DisableTRIAC();
  }
}
