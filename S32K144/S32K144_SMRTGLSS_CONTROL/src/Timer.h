#ifndef TIMER_H_
#define TIMER_H_
/**********************************************************
 * Public Prototypes
 *********************************************************/
void TimerSetup(void);
void TimerUpdate(int duty);
void TimerStart(void);
void TimerStop(void);

#endif /* TIMER_H_ */
