/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Uart.h"

/**********************************************************
 * Configure UART at 9600 bps.
 *********************************************************/
void UartSetup (void)
{
  /* Clocking */
  PCC->PCCn[PCC_LPUART2_INDEX] &= ~PCC_PCCn_CGC_MASK;
  PCC->PCCn[PCC_LPUART2_INDEX] = PCC_PCCn_PCS(1) | PCC_PCCn_CGC_MASK;

  LPUART2->BAUD = LPUART_BAUD_SBR(0x34)   /* Initialize for 9600 baud, 1 stop: */
                  |LPUART_BAUD_OSR(15);   /* SBR=52 (0x34): baud divisor = 8M/9600/16 = ~52 */

  /* Enable transmitter & receiver, no parity, 8 bit char */
  LPUART2->CTRL = LPUART_CTRL_RE_MASK | LPUART_CTRL_TE_MASK;
}

/**********************************************************
 * Transmit character
 *********************************************************/
void UartTx(char send)
{
  /* Wait for transmit buffer to be empty and send character */
  while((LPUART2->STAT & LPUART_STAT_TDRE_MASK) >> LPUART_STAT_TDRE_SHIFT == 0);
  LPUART2->DATA=send;
}

/**********************************************************
 * Checks if received buffer is full,
 * if yes, it returns 1 and store received character in the given address.
 * if not, it returns 0.
 *********************************************************/
char UartRx(char *receive)
{
  char ret;

  if((LPUART2->STAT & LPUART_STAT_RDRF_MASK) >> LPUART_STAT_RDRF_SHIFT == 0)
  {
    /* Received buffer is empty */
    ret = 0;
  }else
  {
    /* Received buffer is full */
    ret = 1;
    *receive = LPUART2->DATA;
  }
 return ret;
}
