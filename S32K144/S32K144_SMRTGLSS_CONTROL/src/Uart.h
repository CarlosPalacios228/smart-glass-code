#ifndef UART_H_
#define UART_H_
/**********************************************************
 * Public Prototypes
 *********************************************************/
void UartSetup (void);
void UartTx (char send);
char UartRx(char *receive);

#endif /* UART_H_ */
