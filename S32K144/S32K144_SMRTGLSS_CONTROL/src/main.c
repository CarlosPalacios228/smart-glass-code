/**********************************************************
 * Includes
 *********************************************************/
#include "device_registers.h"
#include "Clocking.h"
#include "Pins.h"
#include "Uart.h"
#include "Timer.h"
#include "Adc.h"
#include "PID.h"

/* Set below macro = 1, for debugging (Will iterate the POT test) */
#define DEBUG_POT_TEST  (0)

/**********************************************************
 * Smart-Glass Control
 *********************************************************/
int main(void)
{
  char esp8266=0;            /*!< Command from the ESP8266.    */
  uint32_t adcResultInMv=0;  /*!< Voltage in the Potentiometer */
  uint32_t duty_cycle;       /*!< To compute new duty cycle    */
//  uint32_t pid_output;       /*!< Result of the PID loop       */
//  PIDC Control;              /*!< Structure for PID controller */

  /* Configure Clocks, Pins, Timer, ADC & UART */
  NormalRunMode_CoreAt80MHz();
  PORT_Setup();
  TimerSetup();
  ADC_Setup();
  UartSetup();

//  /* PID parameters  (Kp,  Ki,   Kd, T) */
//  PID_Init(&Control,  10, 110,  0.8, 0.001);

#if DEBUG_POT_TEST
  /* Turn on BLUE led */
  PTD->PSOR |= 1<<0 | 1<<15 | 1<<16;
  PTD->PCOR |= 1<<0;

  /* Debug Mode ********************************************************************
   * - Potentiometer ADC readings & perform PWM conversion.
   */
  for(;;)
  {
    ADC_Convert(12);            /* Convert Channel AD12 to pot on EVB     */
    while(!ADC_Complete()) { }  /* Wait for conversion complete flag      */
    adcResultInMv = ADC_Read(); /* Get channel's conversion results in mV */

    /* Scale the 5000mV to a range of 0-21% */
    duty_cycle = (adcResultInMv * 21) / 5000;
    TimerUpdate(duty_cycle);
  }
#else
  /* Endless loop */
  for(;;)
  {
    /* Automatic Mode **************************************************************
     * - Sensors ADC readings (Light LDR's).
     * - Compute the PID loop.
     * - Update TRIAC Timeout
     */
    if(SMG_AUTO == OperationMODE())
    {
      ADC_Convert(6);             /* Convert CH6 (PTB2: LDR1)               */
      while(!ADC_Complete()) { }  /* Wait for conversion complete flag      */
      adcResultInMv = ADC_Read(); /* Get channel's conversion results in mV */

//      /* Compute AVG sensor's results          */
//      ADC_Convert(7);             /* Convert CH7 (PTB3: LDR2)               */
//      while(!ADC_Complete()) { }  /* Wait for conversion complete flag      */
//      adcResultInMv = (adcResultInMv + ADC_Read()) >> 1;

      /* Compute PID Controller: SetPoint = 13, Vin = adcResultInMv */
      duty_cycle = (adcResultInMv * 21) / 5000;
//      pid_output = PID_Controller(&Control, 13, duty_cycle);

      /* Update TRIAC Timeout */
      TimerUpdate(duty_cycle);

      /* Turn-on BLUE led */
      PTD->PSOR |= 1<<0 | 1<<15 | 1<<16;
      PTD->PCOR |= 1<<0;

    } else
      /* Manual Mode ***************************************************************
     * - Serial Communication to the ESP8266 (WiFi).
     * - Receive transparency level from ESP8266.
     * - Update PWM duty cycle.
     */
    if(SMG_MANUAL == OperationMODE())
    {
      /* New Value requested? */
      if(UartRx(&esp8266))
      {
        if(esp8266 <= 100)
        {
          UartTx(esp8266);
          /* Update duty cycle: 0-21% */
          duty_cycle = (esp8266 * 21) / 100;
          TimerUpdate(duty_cycle);
        }
      }

      /* Turn on GREEN led */
      PTD->PSOR |= 1<<0 | 1<<15 | 1<<16;
      PTD->PCOR |= 1<<16;
    }
  }
#endif

return(0);
}
